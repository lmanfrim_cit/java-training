package com.ciandt.training.threads;

public class MyThread extends Thread{

        public void run() {
            for (int i = 0; i< 10; i++){
                System.out.println("MyThread " + i);
                try{
                    Thread.sleep(500);
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        }

    class MyThread2 extends Thread {
        public void run() {
            for (int i = 0; i< 10; i++){
                System.out.println("MyThread2 " + i);
                try{
                    Thread.sleep(500);
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        }
    } //Code

    public static void main(String[] args){
        new MyThread().run();
        new MyThread().new MyThread2().run();
    }
}
