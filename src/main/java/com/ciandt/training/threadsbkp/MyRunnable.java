package com.ciandt.training.threads;

public class MyRunnable implements Runnable {
    public void run() {
        for (int i = 0; i< 10; i++){
            System.out.println("MyRunnable " + i);
            try{
                Thread.sleep(500);
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    class MyRunnable2 implements Runnable {
        public void run() {
            for (int i = 0; i< 10; i++){
                System.out.println("MyRunnable2 " + i);
                try{
                    Thread.sleep(500);
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

    public static void main(String[] args){
        new Thread(new MyRunnable()).start();
        new Thread(new MyRunnable().new MyRunnable2()).start();
    }
}
