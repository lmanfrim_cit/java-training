package com.ciandt.training.annotations;

import java.time.LocalDate;

public class Aposentado {

    private String nome;
    @IdadeMinima(valor = 65)
    private LocalDate dataNascimento;

    public Aposentado(String nome, LocalDate dataNascimento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
