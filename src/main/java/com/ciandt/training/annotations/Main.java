package com.ciandt.training.annotations;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class Main {
    public static void main(String[] args) {
        Usuario usuario = new Usuario("Mário", "42198284863", LocalDate.of(2001, Month.MARCH, 06));
        System.out.println("Usuário maior de idade possui no mínimo 18 anos = " + validador(usuario));

        Aposentado aposentado = new Aposentado("José",  LocalDate.of(1935, Month.MARCH, 06));
        System.out.println("Aposentado possui no mínimo 65 anos = " + validador(aposentado));
    }


    // T is meant to be a Type
    // E is meant to be an Element (List<E>: a list of Elements)
    // K is Key (in a Map<K,V>)
    // V is Value (as a return value or mapped value)
    // ? means Any type extending Object (including Object)
    public static <T> boolean validador(T objeto) {
        Class<?> classe = objeto.getClass();
        for (Field field : classe.getDeclaredFields()) {
            if (field.isAnnotationPresent(IdadeMinima.class)) {
                IdadeMinima idadeMinima = field.getAnnotation(IdadeMinima.class);
                try{
                    field.setAccessible(true);
                    LocalDate dataNascimento = (LocalDate) field.get(objeto);
                    return Period.between(dataNascimento, LocalDate.now()).getYears() >= idadeMinima.valor();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
