package com.ciandt.training.annotations;

import java.time.LocalDate;

public class Usuario {

    private String nome;
    private String cpf;
    @IdadeMinima(valor = 18)
    private LocalDate dataNascimento;

    public Usuario(String nome, String cpf, LocalDate dataNascimento) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }
}
