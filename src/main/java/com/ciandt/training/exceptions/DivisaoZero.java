package com.ciandt.training.exceptions;

public class DivisaoZero extends ArithmeticException {

    private static final String MESSAGE = "Não é permitido efetuar divisões por zero";

    public DivisaoZero(){
        super(MESSAGE);
    }

    public DivisaoZero(String message){
        super(MESSAGE);
    }

}
