package com.ciandt.training.serialization.jackson;

import com.ciandt.training.serialization.beans.Job;
import com.ciandt.training.serialization.beans.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static final String PATHNAME = "myJacksonJson.json";

    public static void jacksonSerialize(final Person person) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        // If we want to print the json formatted
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        try {
            // Getting json as String
            final String json = mapper.writeValueAsString(person);
            System.out.println(json);

            // Saving to a file
            final File f = new File(PATHNAME);
            mapper.writerWithDefaultPrettyPrinter().writeValue(f, person);
        } catch (final IOException e) {
            // Ooops, there's something wrong
            e.printStackTrace();
        }
    }

    public static Person deserializeJackson(final File jsonFile) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        Person person = null;

        try {
            person = mapper.readValue(jsonFile, Person.class);
        } catch (IOException e) {
            // Ooops
            e.printStackTrace();
        }

        return person;
    }

    public static void main(String args[]){
        Map<String, Integer> skillsRate = new HashMap<String, Integer>();
        skillsRate.put("influence", 10);
        Job job = new Job("manager", new BigDecimal(10000D), skillsRate );
        Person person = new Person("Alan", 30, Person.Gender.MALE, new Date(), job);
        jacksonSerialize(person);

       final File f = new File(PATHNAME);
       Person personDeserialized = deserializeJackson(f);
       System.out.println("Person Deserialized = " + personDeserialized.toString());
    }

}
