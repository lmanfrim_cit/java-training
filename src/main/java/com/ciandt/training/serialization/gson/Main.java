package com.ciandt.training.serialization.gson;

import com.ciandt.training.serialization.beans.Job;
import com.ciandt.training.serialization.beans.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main {
    private static final String GSON_JSON_FILE_PATH = "myGsonJson.json";

    public static void gsonSerialize(final Person person) {
        //final Gson gson = new Gson();

        // If we want to print the json formatted
         final Gson gson = new GsonBuilder()
                 .setPrettyPrinting()
                 .setDateFormat("yyyy-MM-dd HH:mm:ss")
                 .create();

        // Getting json as String
        final String json = gson.toJson(person);
        System.out.println(json);

        // Saving to a file
        try {
            final Writer writer = new FileWriter(GSON_JSON_FILE_PATH);
            gson.toJson(person, writer);
            writer.close();
        } catch (IOException e) {
            // Ooops, there's something wrong
            e.printStackTrace();
        }
    }

    public static Person deserializeGson(final File jsonFile) {
        final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Person person = null;
        try {
            final Reader reader = new FileReader(jsonFile);
            person = gson.fromJson(reader, Person.class);
        } catch (FileNotFoundException e) {
            // Oooops
            e.printStackTrace();
        }
        return person;
    }

    public static void main(String args[]){
        Map<String, Integer> skillsRate = new HashMap<String, Integer>();
        skillsRate.put("influence", 10);
        Job job = new Job("manager", new BigDecimal(10000D), skillsRate );
        Person person = new Person("Alan", 30, Person.Gender.MALE, new Date(), job);
        gsonSerialize(person);

        final File f = new File(GSON_JSON_FILE_PATH);
        Person personDeserialized = deserializeGson(f);
        System.out.println("Person Deserialized = " + personDeserialized.toString());
    }
}
