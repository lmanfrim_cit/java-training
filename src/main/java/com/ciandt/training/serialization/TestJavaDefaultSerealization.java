package com.ciandt.training.serialization;

import com.ciandt.training.serialization.beans.Demo;

import java.io.*;


public class TestJavaDefaultSerealization {

        public static void main(String[] args) {
            Demo object = new Demo(1, "serialization");
            String filename = "file.ser";

            // Serialization
            try {
                //Saving of object in a file
                FileOutputStream file = new FileOutputStream(filename);
                ObjectOutputStream out = new ObjectOutputStream(file);

                // Method for serialization of object
                out.writeObject(object);

                out.close();
                file.close();

                System.out.println("Object has been serialized");

            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("IOException is caught");
            }


            Demo object1 = null;

            // Deserialization
            try {
                // Reading the object from a file
                FileInputStream file = new FileInputStream(filename);
                ObjectInputStream in = new ObjectInputStream(file);

                // Method for deserialization of object
                object1 = (Demo) in.readObject();

                in.close();
                file.close();

                System.out.println("Object has been deserialized ");
                System.out.println("a = " + object1.a);
                System.out.println("b = " + object1.b);
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("IOException is caught");
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
                System.out.println("ClassNotFoundException is caught");
            }

        }
}
