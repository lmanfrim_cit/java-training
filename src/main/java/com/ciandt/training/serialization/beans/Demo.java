package com.ciandt.training.serialization.beans;

import java.io.Serializable;

public class Demo implements Serializable {

    private static final long serialVersionUID = 6741195420575453460L;

    // Java code for serialization and deserialization
    // of a Java object

    public int a;
    public String b;

    // Default constructor
    public Demo(int a, String b) {
        this.a = a;
        this.b = b;
    }
}
