package com.ciandt.training.serialization.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class Job implements Serializable {

    private static final long serialVersionUID = 6862315526156721323L;

    private String position;
    private BigDecimal salary;
    private Map<String, Integer> skillsRate;

    public Job(){
    }

    public Job(String position, BigDecimal salary, Map<String, Integer> skillsRate) {
        this.position = position;
        this.salary = salary;
        this.skillsRate = skillsRate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Map<String, Integer> getSkillsRate() {
        return skillsRate;
    }

    public void setSkillsRate(Map<String, Integer> skillsRate) {
        this.skillsRate = skillsRate;
    }

    @Override
    public String toString() {
        return "Job{" +
                "position='" + position + '\'' +
                ", salary=" + salary +
                ", skillsRate=" + skillsRate +
                '}';
    }
}
