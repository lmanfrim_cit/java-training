package com.ciandt.training.serialization.beans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Person implements Serializable {

    private static final long serialVersionUID = -4943376684332875623L;

    public Person(){
    }

    public Person(String name, Integer age, Gender gender, Date birthday, Job job) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.birthday = birthday;
        this.job = job;
    }

    public static enum Gender {
        MALE("Male"), FEMALE("Female");

        private String gender;

        Gender(final String gender) {
            this.gender = gender;
        }

        /**
         * @return the gender
         */
        public String getGender() {
            return gender;
        }
    }

    private String name;
    private Integer age;
    private Gender gender;
    private Date birthday;
    private Job job;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Job getJob() {
        return job;
    }


    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", birthday=" + birthday.getTime() +
                ", job=" + job +
                '}';
    }


}
