package com.ciandt.training.innerclass;

public class DiretorExecutivo {
	
	private RelatoriosExecutivo relatorios;

	public DiretorExecutivo() {
		this.relatorios = new RelatoriosExecutivo();
	}

	private static class RelatoriosExecutivo {
		
		public void imprimir() {
			System.out.println("Imprimindo RelatoriosExecutivo...");
		}
	
	}
	
	public static void main(String[] args) {
		DiretorExecutivo.RelatoriosExecutivo relatoriosExecutivo = new DiretorExecutivo.RelatoriosExecutivo();
		relatoriosExecutivo.imprimir();
	}

}
