package com.ciandt.training.innerclass;

public class Outer {
    class Inner {
        void method() {
            System.out.println("method called");
        }
    }
}
