package com.ciandt.training.innerclass;

public class Program extends Outer.Inner {

   public Program() {
        //new Outer().super();
    }

    public static void main(String[] args)  {
        Program p = new Program();
        p.method();
    }
}
