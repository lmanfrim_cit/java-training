package com.ciandt.training.interfaces;

public class CreditCard extends Tax {

	public static final double TAX_PERCENT = 0.045D;
	
	@Override
	public void makePayment() {
		System.out.println("In CreditCard, the payment is done this way");

	}

	@Override
	public double addTaxToOperation() {
		return getTotalPayment() * TAX_PERCENT;
	}

}
