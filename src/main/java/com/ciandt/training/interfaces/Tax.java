package com.ciandt.training.interfaces;

public abstract class Tax implements Payment {
	
	private double totalPayment;
	
	@Override
	public double getTotalPayment() {
		return totalPayment;
	}

	@Override
	public void setTotalPayment(double totalPayment) {
		this.totalPayment = totalPayment;
	}

}
