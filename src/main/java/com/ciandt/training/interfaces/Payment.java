package com.ciandt.training.interfaces;

public interface Payment {
	
	// by default, every attribute is public static final, even if you dont declare
	double MIN_TRANSACTION_VALUE = 1D;
	public static final double MAX_TRNASACTION_VALUE = 5000D;
	
	//by default it is a public abstract method (no need to declare public abstract)
	public abstract void makePayment();
	
	double addTaxToOperation();
	
	double getTotalPayment();
	
	void setTotalPayment(double totalPayment);
	
	/**
     * Default method in interface, introduced in Java 8.
     */
	default public String getNotificanMessage() {
		return "There was an authorized payment of " + getTotalPayment() + " in your account";
	}
		
	/**
     * Static method in interface, introduced in Java 8.
     */
    public static void sayHelloMessage() {
        System.out.println("Starting payment transaction");
    }
}
