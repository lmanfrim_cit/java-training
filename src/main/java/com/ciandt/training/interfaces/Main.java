package com.ciandt.training.interfaces;

public class Main {

	public static void main(String[] args) {
		Main main = new Main();
		// PayPal payment
		Payment paymenPayPal = new PayPal();
		main.doActionsOfPayment(paymenPayPal);

		// CreditCard Payment
		Payment paymentCreditCard = new CreditCard();
		main.doActionsOfPayment(paymentCreditCard);

	}

	public void doActionsOfPayment(Payment payment) {
		if (payment instanceof PayPal) {
			System.out.println("Starting PayPal");
		} else if (payment instanceof CreditCard) {
			System.out.println("Starting CreditCard");
		} else {
			System.out.println("Starting Another provider transaction");
		}
		Payment.sayHelloMessage();
		payment.setTotalPayment(2000D);
		payment.makePayment();
		System.out.println("Total Payment = " + payment.getTotalPayment());
		System.out.println("Tax Operation = " + payment.addTaxToOperation());
		System.out.println("NotificationMessage = " + payment.getNotificanMessage());
		System.out.println();
	}
}
