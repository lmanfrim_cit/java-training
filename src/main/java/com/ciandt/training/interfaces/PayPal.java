package com.ciandt.training.interfaces;

public class PayPal extends Tax {

	public static final double TAX_PERCENT = 0.035D;
	
	
	@Override
	public void makePayment() {
		System.out.println("In PayPal, the payment is done this way");

	}

	@Override
	public double addTaxToOperation() {
		return getTotalPayment() * TAX_PERCENT;
	}

	
}
