package com.ciandt.training.collections;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {
	
	public static void main(String[] args) {
		Map<Integer, String> hashMap = new HashMap<Integer, String>();

		hashMap.put(0, null);
		hashMap.put(1, "Fernando");
		hashMap.put(2, "Alex");
		hashMap.put(3, "José");

		System.out.println(hashMap);

		hashMap.put(0, "Antônio");

		System.out.println(hashMap);

		System.out.println(hashMap.get(3));
	}
}
