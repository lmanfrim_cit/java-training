package com.ciandt.training.generics;

import java.util.List;

public class Example4 {

    // No compilant
    // public void test(List<T extends Number> list){}

    public void test2(List<? extends Number> list){}

    //No compilant
    // class Test<?>{}

    class Test2<T>{}

}
