package com.ciandt.training.generics;

public class GenericsTypeOld {

    private Object t;

    public Object get() {
        return t;
    }

    public void set(Object t) {
        this.t = t;
    }

    public static void main(String args[]){
        GenericsTypeOld type = new GenericsTypeOld();
        type.set("Generics");
        String str = (String) type.get(); //type casting, error prone and can cause ClassCastException

        type.set(1200);
        Integer intNumber = (Integer) type.get();
    }
}
