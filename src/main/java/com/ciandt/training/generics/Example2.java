package com.ciandt.training.generics;

import java.util.ArrayList;
import java.util.List;

public class Example2 {
    public static void main (String[] args){
        //Com Generics
        List<Integer> lista = new ArrayList<Integer>();
        lista.add(1);
        lista.add(2);
        lista.add(3);

        int total = 0;
        for(Integer i : lista){
            total += i;
        }

        System.out.println(total);



        //Sem Generics
        List lista2 = new ArrayList();
        lista2.add(1);
        lista2.add(2);
        lista2.add(3);

        int total2 = 0;
        for (int i = 0; i < lista2.size(); i++){
            total2 += (int) lista2.get(i);
        }

        System.out.println(total2);

    }
}
