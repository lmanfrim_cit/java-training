package com.ciandt.training.generics;

import java.util.ArrayList;
import java.util.List;

public class Example3 {
    public static void main(String[] args){

        //Sem Generics
        List lista = new ArrayList();
        lista.add(1);
        lista.add(2);
        lista.add("3");

        int total = 0;
        for (int i = 0; i < lista.size(); i++){
            total += (int) lista.get(i);
        }

        System.out.println(total);

    }
}
