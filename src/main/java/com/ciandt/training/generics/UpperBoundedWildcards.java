package com.ciandt.training.generics;

import java.util.ArrayList;
import java.util.List;

public class UpperBoundedWildcards {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(3); ints.add(5); ints.add(10);
        double sum = sum(ints);
        System.out.println("Sum of ints="+sum);

        List<Double> doubles = new ArrayList<>();
        doubles.add(3.3D); doubles.add(5.5); doubles.add(10.10);
        double sum2 = sum(doubles);
        System.out.println("Sum of doubles="+sum2);
    }

    //Upper bounded
    public static double sum(List<? extends Number> list){
        double sum = 0;
        for(Number n : list){
            sum += n.doubleValue();
        }
        return sum;
    }
}
