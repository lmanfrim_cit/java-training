package com.ciandt.training.generics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UnboundedWildcards {

    public static void main (String[] args){
        List<Integer> ints = new ArrayList<>();
        ints.add(1); ints.add(2);
        printData(ints);

        List<String> strings = new ArrayList<>();
        strings.add("Um"); strings.add("Dois");
        printData(strings);

        List<File> files = new ArrayList<>();
        files.add(new File("a.txt"));
        files.add(new File("b.txt"));
        printData(files);
    }

    public static void printData(List<?> list){
        for(Object obj : list){
            System.out.println(obj);
        }
    }
}
