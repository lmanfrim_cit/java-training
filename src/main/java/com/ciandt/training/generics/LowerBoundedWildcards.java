package com.ciandt.training.generics;

import java.util.ArrayList;
import java.util.List;

public class LowerBoundedWildcards {

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        System.out.println("Lista 1 = " + addNumbers(ints));

        List<Number> numbers = new ArrayList<>();
        System.out.println("Lista 2 = " + addNumbers(numbers));

        List<Object> objects = new ArrayList<>();
        System.out.println("Lista 3 = " +addNumbers(objects));
    }

    public static List<? super Integer> addNumbers(List<? super Integer> list) {
        for (int i = 1; i <= 10; i++) {
            list.add(i);
        }
        return list;
    }
}
