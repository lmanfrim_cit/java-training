package com.ciandt.training.dojos.turma2.snake;

import com.ciandt.training.dojos.turma2.snake.enums.Direcao;

import java.io.IOException;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {

        int maxX = 20;
        int maxY = 20;

        Tela tela = new Tela(maxX,maxY);

        Pixel pixel  = new Pixel(maxX/2, maxY/2);
        Snake snake = new Snake(pixel);

        Maca maca = new Maca(pixel);

        int timeMaca = 0;

        while (true){
            limparTela();
            tela.montaTela();
            tela.incluirObjeto(snake);
            tela.printaTela();
            Direcao direcao = obtemDirecao();
            snake.mover(direcao);
            snake.crescer();

            if (timeMaca == 3){
                pixel  = new Pixel(3, 3);
                maca.setPixel(pixel);
                maca.setVisible(true);
            }
            timeMaca ++;

            if (maca.isVisible()) {
                tela.incluirObjeto(maca);
            }

            if (snake.colisao(maca)) {
                maca.setVisible(false);
            }


            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static Direcao obtemDirecao() {
        System.out.println("Digite: W, A, S ou D");
        Direcao direcao = null;
        while(direcao == null) {
            Scanner scanner = new Scanner(System.in);

            String valor = scanner.next();

            try {
                switch (valor) {
                    case "a":
                    case "A":
                        direcao = Direcao.ESQUERDA;
                        break;

                    case "s":
                    case "S":
                        direcao = Direcao.BAIXO;
                        break;

                    case "w":
                    case "W":
                        direcao = Direcao.CIMA;
                        break;

                    case "d":
                    case "D":
                        direcao = Direcao.DIREITA;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return  direcao;
    }

    private static void limparTela() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}
