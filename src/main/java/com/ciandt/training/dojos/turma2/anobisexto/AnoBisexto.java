package com.ciandt.training.dojos.turma2.anobisexto;

public class AnoBisexto {

    public static void main (String[] args)
    {
        int ano = 4400;

        if ( ano % 4 != 0) {
            System.out.println("Esse ano NÃO É bissexto");
        }
        else {
            if (ano % 100 != 0) {
                System.out.println("Esse ano É bissexto");
            } else if (ano % 400 == 0) {
                System.out.println("Esse ano É bissexto");
            } else {
                System.out.println("Esse ano  NÃO É bissexto");
            }

        }
    }


}

/*
São bissextos por exemplo:
1600
1732
1888
1944
2008
Não são bissextos por exemplo:
1742
1889
1951
2011


 */