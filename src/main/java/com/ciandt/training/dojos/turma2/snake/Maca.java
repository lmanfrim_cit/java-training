package com.ciandt.training.dojos.turma2.snake;

import java.util.ArrayList;

public class Maca extends Objeto {

    private boolean visible;

    public Maca(Pixel px){

        this.pixels = new ArrayList();

        this.pixels.add(px);

        this.visible = false;

        this.simbol = 'o';

    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setPixel(Pixel pixel) {
        this.pixels.clear();
        this.pixels.add(pixel);
    }
}
