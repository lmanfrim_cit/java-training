package com.ciandt.training.dojos.turma2.snake;

import com.ciandt.training.dojos.turma2.snake.enums.Direcao;

import java.util.ArrayList;

public class Snake extends Objeto {

 static Direcao direcao;

 private Pixel ultimoAnterior;

    public Snake(Pixel px) {
        this.pixels = new ArrayList();

        this.pixels.add(px);

        this.simbol = 'X';

        //TODO Random DIRECAO
        direcao = Direcao.DIREITA;
    }


    public void mover(Direcao direcao) {
        Pixel ultimoPixel = pixels.get(pixels.size() - 1);;
        ultimoAnterior = new Pixel(ultimoPixel.getX(), ultimoPixel.getY());

        Pixel primeiroPixel = pixels.get(0);
        pixels.remove(ultimoPixel);

        switch (direcao) {
            case CIMA:
                ultimoPixel.setY(primeiroPixel.getY() - 1);
                ultimoPixel.setX(primeiroPixel.getX());
                break;
            case BAIXO:
                ultimoPixel.setY(primeiroPixel.getY() + 1);
                ultimoPixel.setX(primeiroPixel.getX());
                break;
            case ESQUERDA:
                ultimoPixel.setY(primeiroPixel.getY());
                ultimoPixel.setX(primeiroPixel.getX() - 1);
                break;
            case DIREITA:
                ultimoPixel.setY(primeiroPixel.getY());
                ultimoPixel.setX(primeiroPixel.getX() + 1);
                break;
        }

        pixels.add(0, ultimoPixel);

    }


    public void crescer() {

        double teste = Math.random();

        if (teste > 0.5) {

            if (ultimoAnterior != null) {
                pixels.add(ultimoAnterior);
            }
        }
     }
}
