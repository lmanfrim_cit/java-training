package com.ciandt.training.dojos.turma2.snake;

public class Tela {

    private int lenX = 60;
    private int lenY = 60;

    private char[][] area;

    public Tela(int maxX, int maxY){

        lenX = maxX;
        lenY = maxY;

        area = new char[lenY][lenX];

        montaTela();
    }

    public void montaTela() {

        for (int y=0; y<lenY; y++){
            for (int x = 0; x < lenY; x++) {
                if (y == 0 || y == lenY-1) {
                    area[y][x] = '-';
                }
                else if(x == 0 || x == lenX-1){
                    area[y][x] = '|';
                }
                else{
                    area[y][x] = ' ';
                }
            }
        }
    }

    public void printaTela() {
        for (int i=0; i<lenY; i++){
            for (int j=0; j<lenX; j++) {
                System.out.print(area[i][j]);
            }
            System.out.print("\n");
        }
    }

    public void incluirObjeto(Objeto obj) {

        for (Pixel pixel: obj.pixels) {
            area[pixel.getY()][pixel.getX()] = obj.simbol;
        }

    }

}
