package com.ciandt.training.regex;

import java.util.regex.*;

public class Regex {
    public static void main(String[] args) {

        String fonte = "mouse cor azul e botão cor vermelho!";
        String queremosIsso = "cor";

        Pattern p = Pattern.compile(queremosIsso);
        Matcher m = p.matcher(fonte);
        while (m.find()) {
            System.out.println(m.start() + " " + m.group() + " " + m.end());
        }

        String resultado = m.replaceFirst("teste");
        System.out.println(resultado);

        resultado = m.replaceAll("colorido");
        System.out.println(resultado);


        System.out.println("###############################################");

        String patternEmail = "^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+.[A-Za-z]$";
        String[] emails = { "teste@teste@com", "teste.teste@teste.com", "Fred Bloggs@example.com", "user@.invalid.com", "gmail@chucknorris.com", "webmaster@müller.de", "matteo@78.47.122.114" };

        p = Pattern.compile(patternEmail, Pattern.CASE_INSENSITIVE);
        for (String email : emails) {
            System.out.println(email + " : " + (p.matcher(email).matches() ? "valido" : "invalido"));
        }

        System.out.println("###############################################");

        String patternTelefoneComDDD = "[(]?\\d\\d[)]? \\d{4}[-]?\\d{4}";
        String[] tels = { "(11) 35120000", "11 35120000", "35120000", "11 3512-0000", "(11) 3512-0000", "3512-0000", "11\n35121234" };

        p = Pattern.compile(patternTelefoneComDDD);
        for (String tel : tels) {
            System.out.println(tel + " : " + (p.matcher(tel).matches() ? "valido" : "invalido"));
        }

        System.out.println("###############################################");

        String patternX = "a(?!b).";
        p = Pattern.compile(patternX);
        String[] textos = { "abc", "acb", "aabbcc", "abbc", "a1b2c3", "1abacadab2", "ABC", "ACB" };
        for (String texto : textos) {
            m = p.matcher(texto);
            System.out.println(texto);
            while (m.find()) {
                System.out.println(m.start() + " " + m.group() + " " + m.end());
            }
            System.out.println("*****************");
        }

        String patternZX = "^.{2}(?!9)";


    }
}
