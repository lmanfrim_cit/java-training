package com.ciandt.training.abstractclass;

public abstract class LoginAuth {

	// Implement the same default behavior here
	// that is shared by all subclasses.
	public String encryptPassword(String pass) {
		String encrypt = "Doing very cleaver encrypting " + pass;
		System.out.println(encrypt);
		return encrypt;
	}

	// Each subclass needs to provide their own implementation of this only:
	public abstract void checkDBforUser();
}
