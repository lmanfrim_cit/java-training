package com.ciandt.training.abstractclass;

public class DBOracleExpressEdition extends DBOracle {

	@Override
	public void checkDBforUser() {
		System.out.println("In OracleExpressEdition, we are so cheap that we don't check users.");
	}

	@Override
	public String encryptPassword(String pass) {
		String encrypt = "In OracleExpressEdition, we have a weak encrypt password method... "
				+ "So sorry for your pass " + pass;
		System.out.println(encrypt);
		return encrypt;
	}

}
