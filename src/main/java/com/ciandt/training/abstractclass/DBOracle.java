package com.ciandt.training.abstractclass;

public class DBOracle extends LoginAuth{

	@Override
	public void checkDBforUser() {
		System.out.println("In Oracle, that's how we check users");		
	}

}
