package com.ciandt.training.abstractclass;

public class Main {

	public static void main(String[] args) {
		LoginAuth loginAuthMySQL = new DBMySQL();
		LoginAuth loginAuthOracle = new DBOracle();
		LoginAuth loginAuthOracleExpress = new DBOracleExpressEdition();
		
		Main main = new Main();
		
		//MySQL
		System.out.println("==========MySQL==========");
		main.doDatabaseUserOperations("423@ewq321dsDESA", loginAuthMySQL);
		
		
		//Oracle
		System.out.println("==========Oracle==========");
		main.doDatabaseUserOperations("D!S@87$#ASD5", loginAuthOracle);
		
		//OracleXE
		System.out.println("==========OracleXE==========");
		main.doDatabaseUserOperations("abc123", loginAuthOracleExpress);
		
	}
	
	public void doDatabaseUserOperations(String pass, LoginAuth loginAuth) {
		loginAuth.checkDBforUser();
		loginAuth.encryptPassword(pass);
	}
}
