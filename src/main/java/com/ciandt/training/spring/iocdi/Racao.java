package com.ciandt.training.spring.iocdi;

public class Racao implements Comestivel {

    @Override
    public void comer() {
        System.out.println("Bidu comeu ração");
    }
}
