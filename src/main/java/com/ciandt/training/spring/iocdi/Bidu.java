package com.ciandt.training.spring.iocdi;

public class Bidu {

    public String nome;
    public Comestivel comestivel;

    public Bidu() {
    }

    public Bidu(Comestivel comestivel) {
        super();
        this.comestivel = comestivel;
    }

    public void acorda() {
        System.out.println("Bidu acordou");
    }

    public void comer() {
        comestivel.comer();
    }

}