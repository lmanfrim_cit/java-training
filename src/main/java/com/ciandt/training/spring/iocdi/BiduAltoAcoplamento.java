package com.ciandt.training.spring.iocdi;

public class BiduAltoAcoplamento {

    public String nome;
    public Comestivel comestivel;

    public BiduAltoAcoplamento() {
    }

    public BiduAltoAcoplamento(Comestivel comestivel) {
        super();
        this.comestivel = comestivel;
    }

    public void acorda() {
        System.out.println("Bidu acordou");
    }

    public void comer() {
        Racao racao = new Racao();
        racao.comer();
    }
}
