package com.ciandt.training.spring.iocdi;

public class Main {

    public static void main(String[] args) {
        Comestivel pao = new Racao();
        Bidu bidu = new Bidu(pao);

        bidu.acorda();
        bidu.comer();
    }
}
